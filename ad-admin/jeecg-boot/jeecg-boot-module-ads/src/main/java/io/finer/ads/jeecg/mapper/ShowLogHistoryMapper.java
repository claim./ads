package io.finer.ads.jeecg.mapper;

import io.finer.ads.jeecg.entity.ShowLogHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 呈现日志-历史
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
public interface ShowLogHistoryMapper extends BaseMapper<ShowLogHistory> {

}
